FROM debian:stretch
RUN apt-get update && apt-get install -y \
	openscad \
	slic3r \
	freeglut3 \
	libgtk2.0-0 \
	libxmu6 \
	libboost-geometry-utils-perl \
	libc6 \
	libencode-locale-perl \
	libfile-spec-perl \
	libio-stringy-perl \
	libmath-convexhull-monotonechain-perl \
	libmath-geometry-voronoi-perl \
	libmath-planepath-perl \
	libmoo-perl \
	libstorable-perl \
	libtime-hires-perl

CMD /bin/bash
